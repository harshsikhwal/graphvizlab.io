---
redirect_from:
  - /_pages/Gallery/directed/Linux_kernel_diagram.html
layout: gallery
title: Linux Kernel Diagram
svg: Linux_kernel_diagram.svg
copyright: Copyright Constantine Shulyupin, licensed under EPL
gv_file: Linux_kernel_diagram.gv.txt
img_src: Linux_kernel_diagram.png
---
Contributed by Constantine Shulyupin. This is a simpler version of https://makelinux.github.io/kernel\_map/. Source code originated at https://github.com/makelinux/linux\_kernel\_map/blob/master/Linux\_kernel\_diagram.dot
