---
copyright: Copyright &#169; 1996-2004 AT&amp;T.  All rights reserved.
redirect_from:
  - /_pages/Gallery/gradient/g_c_n.html
layout: gallery
title: Graph, Cluster and Node Gradients
svg: g_c_n.svg
gv_file: g_c_n.gv.txt
img_src: g_c_n.png
---
Demonstrates graph, cluster and node gradients.
