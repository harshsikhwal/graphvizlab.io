---
defaults: []
flags:
- write
minimums: []
name: lwidth
types:
- double
used_by: GC
---
Width of graph or cluster label, in inches.
