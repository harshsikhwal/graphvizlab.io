---
defaults:
- '""'
flags: []
minimums: []
name: comment
types:
- string
used_by: ENG
---
Comments are inserted into output. Device-dependent
