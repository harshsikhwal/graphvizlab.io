---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: headhref
types:
- escString
used_by: E
---
Synonym for [`headURL`](#d:headURL).
