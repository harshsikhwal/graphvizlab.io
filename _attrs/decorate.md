---
defaults:
- 'false'
flags: []
minimums: []
name: decorate
types:
- bool
used_by: E
---
If true, attach edge label to edge by a 2-segment
polyline, underlining the label, then going to the closest point of spline.
