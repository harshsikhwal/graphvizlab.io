---
defaults:
- 'false'
flags: []
minimums: []
name: center
types:
- bool
used_by: G
---
If true, the drawing is centered in the output canvas.
