---
defaults:
- '0'
flags:
- sfdp
minimums: []
name: rotation
types:
- double
used_by: G
---
Rotates the final layout counter-clockwise by the specified number of degrees.
