---
name: Xlib canvas
params:
- xlib
- x11
---
Creates an [Xlib](http://en.wikipedia.org/wiki/Xlib) window and displays the output there.
