---
name: GD/GD2 formats
params:
- gd
- gd2
---
Output images in the GD and GD2 format. These are the internal
formats used by the gd library. The latter is compressed.
