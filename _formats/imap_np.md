---
name: Server-side and client-side imagemaps
params:
- imap_np
- cmapx_np
---
These are identical to the `imap` and `cmapx` formats, except they
rely solely on rectangles as active areas.
